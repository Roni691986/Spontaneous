/*
 * ********************* Main Controller ****************
 */

app.controller('mainController',function ($scope, $http , $rootScope , $timeout ) {

	angular.element('.carousel').carousel({
		interval:8000
		})
	$scope.showFlightResults  = false;
	$scope.sortType = "fare.total_price";
	$scope.flight = {
			origin : "TLV",
			origin_timezone : "",
			destination : "LAX",
			destination_timezone : ""
	};

	//get the timezone diffrences
	$http.get("http://api.fixer.io/latest?base="+$rootScope.currencyCoin)
	.then(function(response) {
		console.log(response.data);
		$scope.coins = response.data;
	});
	
	$http.get("content/json/timeZoneByAirports.json")
	.then(function(response) {
		response.data.forEach(function(timeZone) {
			if (timeZone.code =='TLV'){
				$scope.flight.origin_timezone = timeZone.offset.gmt
			}
			if (timeZone.code =='LAX'){
				$scope.flight.destination_timezone = timeZone.offset.gmt
			}
		})
	});

	$scope.$on('openFlightsEvent', function(event, args) {
		console.log(args.value);
		//TODO: get flights with args.value (TLV for example)
		$http.get("content/json/flights.json")
		.then(function(response) {
			$scope.flights = response.data.results;
		});
	});

	$scope.splitHour = function(date){
		if (date !== null && date !== undefined){
			var array = date.split('T');
			return array[1];
		}
	}

	//TODO: check why the return not working - duration time.
	$scope.calcFlightTime = function(returnFrom,depart,arrive){
		if (depart !== null && depart !== undefined && arrive !== null && arrive !== undefined){
			var difference = ((new Date(arrive)) - (new Date(depart)));
			var diff = new Date(difference / (60000)*1000).toUTCString().split(" ")[4];
			//return returnResult =  diff.split(":")[1] + "h" + diff.split(":")[2] + "m";
			if (returnFrom =='local'){
				return parseInt(Math.abs($scope.flight.origin_timezone - $scope.flight.destination_timezone)) + parseInt(diff.split(":")[1]) + "h:" + diff.split(":")[2] + "m";
			}
			else
				return parseInt(diff.split(":")[1]) + "h:" + diff.split(":")[2] + "m";
		}
	}

	$scope.searchFlights = function(){
		console.log("search! get the date and origin");
		$scope.showFlightResults = true;
	}

	$scope.popPrice = function(){
		console.log("a");
	}
	$scope.changePassengers = function(num,type){
		if (type == 'adult')
			$rootScope.adultPassengers = num;
		else
			$rootScope.childPassengers = num;
		
		//TODO: add in order to hide on select pessengers number!
		//$('[data-toggle="popover"]').each(function () {$(this).popover('hide');});
		$rootScope.passengers = String($rootScope.adultPassengers + $rootScope.childPassengers);
		$(".passengersSum > span").html(String($rootScope.passengers));
	}
	$scope.openPessengerPopup = function(num,type){
		$(".personsPopover").show();
	}
	$scope.changeCurrency = function(coinType){
		$rootScope.currencyCoin = coinType;
	}


});





