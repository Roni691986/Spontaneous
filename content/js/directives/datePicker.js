app.controller('datePicker', function($scope) {
	this.myDate = new Date();
	
	$scope.changeDate = function(dateObj){
		var month = dateObj.getUTCMonth() + 1; //months from 1-12
		var day = dateObj.getUTCDate() + 1;
		var year = dateObj.getUTCFullYear();

		newdate = day + "/" + month + "/" + year;
		console.log(newdate);
		
	
	}
	this.minDate = new Date(
			this.myDate.getFullYear(),
			this.myDate.getMonth(),
			this.myDate.getDate()
	);

	this.maxDate = new Date(
			this.myDate.getFullYear(),
			this.myDate.getMonth() + 1,
			this.myDate.getDate()
	);

	this.onlyWeekendsPredicate = function(date) {
		var day = date.getDay();
		return day === 0 || day === 6;
	};
});