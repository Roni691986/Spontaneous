"use strict";
app.controller('autocomplete',function ($scope, $http, $timeout, $q, $log) {

	var self = this;

	self.simulateQuery = false;
	self.isDisabled    = false;
	self.states        = loadAll();
	self.querySearch   = querySearch;
	self.selectedItemChange = selectedItemChange;
	self.searchTextChange   = searchTextChange;

	/**
	 * Search for states... use $timeout to simulate
	 * remote dataservice call.
	 */

	function loadAll() {

		var allStates = [];
		return $http.get("./content/json/airport.json").then(function(response) {
			$.each(response.data,function(index, value){
				allStates.push(value.country);
			});
			return allStates.map( function (state) {
				return {
					value: state.toLowerCase(),
					display: state
				};
			});
		})

	}

	function querySearch (query) {
		var jsonStates = self.states.$$state.value;
		var results = query ? jsonStates.filter( createFilterFor(query) ) : jsonStates, deferred;
		return results;
	}

	function searchTextChange(text) {
		$log.info('Text changed to ' + text);
	}

	function selectedItemChange(item) {
		if (item !==undefined){
			$log.info('Item changed to ' + JSON.stringify(item));
			$scope.$emit('openFlightsEvent',item);
		}
	}

	function createFilterFor(query) {
		var lowercaseQuery = angular.lowercase(query);

		return function filterFn(state) {
			return (state.value.indexOf(lowercaseQuery) === 0);
		};


	}
});