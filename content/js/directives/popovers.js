app.directive('budgetPopover', function ($compile , $rootScope) {
	return {
		restrict: 'A',
		replace: true,
		scope:{ popoverHtml:'@', budget:'@' , currencyCoin: '@'}, // Isolated the scope 
		template: function(element, attrs) {
			var currencyCoin = $rootScope.currencyCoin.toLowerCase();
         return '<span  budget="{{budget}}" ><i class="fa fa-'+currencyCoin+' popupCoin" aria-hidden="true"></i> {{budget}}</span>';
			},
		link: function (scope, el, attrs) {
			scope.label = attrs.popoverLabel;
			$(el).popover({
				trigger: 'click',
				html: true,
				placement: attrs.popoverPlacement,
				content: $compile( '<div range-slider min="0" max="1500" model-min="'+$rootScope.budget+'" model-max="1000" attach-handle-values="true" step="50" filter="currency"  filter-options="'+$rootScope.coinsAscii[$rootScope.currencyCoin]+'"></div>' )(scope)
			});
			scope.$watch("budget",function(newValue,oldValue) {
		    });

			
		}
			    
	};
});



app.directive('originPopover', function ($compile , $rootScope) {
	var htmlInputInclude = "<div><ng-include src=\"'content/pages/autoCompleteCountries.html'\"></ng-include></div>";
	return {
		restrict: 'A',
		replace: true,
		scope:{ popoverHtml:'@'}, // Isolated the scope 
		template: '<span><i class="fa fa-globe" aria-hidden="true"></i> From: TLV</span>',
		link: function (scope, el, attrs) {
			scope.label = attrs.popoverLabel;
			$(el).popover({
				trigger: 'click',
				html: true,
				placement: attrs.popoverPlacement,
				content: $compile(htmlInputInclude)(scope)
			});  
		}
	};
});
app.directive('personsPopover', function ($compile , $rootScope) {
	var passengersNum = String($rootScope.passengers);
	return {
		restrict: 'A',
		replace: true,
		template: '<span class="passengersSum" ng-click="openPessengerPopup()"><i class="fa fa-users" aria-hidden="true"></i><span>'+passengersNum+'</span></span>',
		link: function (scope, el, attrs ) {
			$(el).popover({
				trigger: 'click',
				html: true,
				placement: attrs.popoverPlacement,
				content: $(".personsPopover")
			});  
		}
	};
});

