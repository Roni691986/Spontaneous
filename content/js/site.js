/** **********************************ANGULAR*************************************** */
var app = angular.module('spontaniousApp', ["ngRoute" ,"ngMaterial" , 'ngMessages' , 'ui-rangeSlider']);

app.config(function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'content/pages/main.html',
		controller: 'mainController'
	})

});

app.run(function($rootScope) {
	$rootScope.passengers = "2";
	$rootScope.adultPassengers = 2;
	$rootScope.childPassengers = 0;
	$rootScope.budget = 150;
	$rootScope.currencyCoin = "JPY";
	
	$rootScope.coinsAscii = {
		    'USD': '$', // US Dollar
		    'ILS': "\u20AA", // Israeli New Sheqel
		    'EUR': '\u20AC', // Euro
		    'GBP': '\u00a3', // British Pound Sterling
		    'JPY': '\u00a5', // Japanese Yen
		};
	
})




//JS FUNCTIONS
$(function() {
	
});

$(document).on('click', '.btn-select', function (e) {
	e.preventDefault();
	var ul = $(this).find("ul");
	if ($(this).hasClass("active")) {
		if (ul.find("li").is(e.target)) {
			var target = $(e.target);
			target.addClass("selected").siblings().removeClass("selected");
			var value = target.html();
			$(this).find(".btn-select-input").val(value);
			$(this).find(".btn-select-value").html(value);
		}
		ul.hide();
		$(this).removeClass("active");
	}
	else {
		$('.btn-select').not(this).each(function () {
			$(this).removeClass("active").find("ul").hide();
		});
		ul.slideDown(300);
		$(this).addClass("active");
	}
});

$('body').on('click', function (e) {
	$('[data-toggle="popover"]').each(function () {
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			$(this).popover('hide');
		}
	});
});
